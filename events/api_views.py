from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Conference, Location, State
import json
from events.acls import get_location_photo, get_weather_data


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "img_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "room_count", "state"]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name", "starts", "location"]

    encoders = {
        "location": LocationDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        print(f"conference = {conference}")
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        content = json.loads(request.body)
        print(f"content = {content}")
        # print(f"CONTENT = {content}")
        # state = State.objects.get(abbreviation=content["state"])
        # print(f"STATE = {state}")
        # content["state"] = state
        # print(f"content = {content}")
        # print(f"STATE NAME = {state.abbreviation}")
        # print(f"THE SECOND CONTENT = {content}")
        # content = json.loads(request.body)
        weather = get_weather_data(content["city"], content["state"])

        return JsonResponse(
            {
                "conference": conference,
                "weather": weather,
            },
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # PUT
        content = json.loads(request.body)
        print(content)
        # if "href" in content:
        #     href = Conference.objects.get(href=content["href"])
        #     content["href"] = href

        # Conference.objects.filter(id=id).update(**content)

        # copied from get detail

        conference = Conference.objects.filter(id=id)
        conference.update(**content)

        # # conference.update()

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()

        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_location_photo(
            content["city"], content["state"].abbreviation
        )
        content.update(photo)

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content[
                    "state"
                ] = state  # these two lines turn the value into python object "state": <State: New York>,
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# ====================================================================


# """
# API LIST LOCATION
# Lists the conference names and the link to the conference.

# Returns a dictionary with a single key "conferences" which
# is a list of conference names and URLS. Each entry in the list
# is a dictionary that contains the name of the conference and
# the link to the conference's information.

# ====================================================================


# {
#     "conferences": [
#         {
#             "name": conference's name,
#             "href": URL to the conference,
#         },
#         ...
#     ]
# }
# """

# """
# API SHOW LOCATION
# Returns the details for the Location model specified
# by the id parameter.

# This should return a dictionary with the name, city,
# room count, created, updated, and state abbreviation.
# """

# ====================================================================


# API LIST LOCATION
# """
# Lists the location names and the link to the location.

# Returns a dictionary with a single key "locations" which
# is a list of location names and URLS. Each entry in the list
# is a dictionary that contains the name of the location and
# the link to the location's information.
# """

# ====================================================================


# API SHOW CONFERENCE
# """
# Returns the details for the Conference model specified
# by the id parameter.

# This should return a dictionary with the name, starts,
# ends, description, created, updated, max_presentations,
# max_attendees, and a dictionary for the location containing
# its name and href.

# {
#     "name": the conference's name,
#     "starts": the date/time when the conference starts,
#     "ends": the date/time when the conference ends,
#     "description": the description of the conference,
#     "created": the date/time when the record was created,
#     "updated": the date/time when the record was updated,
#     "max_presentations": the maximum number of presentations,
#     "max_attendees": the maximum number of attendees,
#     "location": {
#         "name": the name of the location,
#         "href": the URL for the location,
#     }
# }
# """


# @require_http_methods(["GET", "POST"])
# def api_list_locations(request):
#     if request.method == "GET":
#         locations = Location.objects.all()

#         return JsonResponse(
#             {"locations": locations}, encoder=LocationListEncoder
#         )
#     else:  # request is POST
#         content = json.loads(request.body)  # get the data from the 3rd party

#         try:
#             state = State.objects.get(
#                 abbreviation=content["state"]
#             )  # from the State model (db) retrieve, the abbreviation that corresponds to the 3rd party APIs "state variable"
#             content[
#                 "state"
#             ] = state  # update the response body dictionary with the actual instance of the State object (defined in teh line above);; this comes in handy later when we use the .abbreviation later
#             # '''# now the content variable looks like this vvvvv below vvvvv
#             # {
#             #         "city": "New York City",
#             #         "state": <State: New York>,
#             #         # Other key-value pairs in the content dictionary
#             #     }
#             # '''

#         except State.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid state abbreviation"},
#                 status=400,
#             )
#         photo = get_location_photo(
#             content["city"],
#             content[
#                 "state"
#             ].abbreviation,  # we can now access the .abbreviation because we previously transformed the state string in a python object
#         )
#         content.update(photo)

#         location = Location.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )
