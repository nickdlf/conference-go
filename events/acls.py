from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import requests
import json


# PEXELS_API_KEY = "Ovbrqpd8XciROPwtIQ22jsCZuyZ0ebx83Or9X1sNuQdn58JO3useh8Bi"
# OPEN_WEATHER_API_KEY = "f0158dd2b1d9568e5529f124e8180cb1"


# def get_photo(city, state): *******my attempt*******
#     # Create a dictionary for the headers to use in the request
#     headers = {
#         "Authorization": PEXELS_API_KEY,
#     }
#     # Create the URL for the request with the city and state
#     url = f"https://api.pexels.com/v1/search?query={city}+{state}"

#     # Make the request
#     response = requests.get(url, headers=headers)

#     # Parse the JSON response
#     image_data = json.loads(response.content)
#     # Return a dictionary that contains a `picture_url` key and
#     #   one of the URLs for one of the pictures in the response
#     picture = {
#         # "name": image_data["name"],
#         # "city": image_data["city"],
#         # "room_count": image_data["room_count"],
#         # "created": image_data["created"],
#         # "updated": image_data["updated"],
#         # "state": image_data["state"],
#         "img_url": image_data["img_url"],
#     }
#     return picture


# def weather_data(city, state, country):
#     # Create the URL for the geocoding API with the city and state
#     geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit={2}&appid={OPEN_WEATHER_API_KEY}"

#     # Make the request
#     # Parse the JSON response
#     # Get the latitude and longitude from the response

#     # Create the URL for the current weather API with the latitude
#     #   and longitude
#     # Make the request
#     # Parse the JSON response
#     # Get the main temperature and the weather's description and put
#     #   them in a dictionary
#     # Return the dictionary


# their solution


def get_location_photo(city, state):
    try:
        url = "https://api.pexels.com/v1/search"
        params = {
            "query": f"{city} {state}",
            "per_page": 1,
        }
        headers = {"Authorization": PEXELS_API_KEY}

        res = requests.get(url, params=params, headers=headers)
        unencoded = json.loads(res.content)

        url = unencoded["photos"][0]["url"]
        return {"img_url": url}
    except (KeyError, IndexError):
        return {"img_url": None}


def get_weather_data(city, state):
    try:
        url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
        res = requests.get(url)
        unencoded = json.loads(res.content)
        lat = unencoded[0]["lat"]
        lon = unencoded[0]["lon"]
    except IndexError:
        return None

    try:
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        res = requests.get(url)
        unencoded = json.loads(res.content)
        description = unencoded["weather"][0]["description"]
        temp = unencoded["main"]["temp"]
        return {"description": description, "temp": temp}

    except IndexError:
        return None


# except (KeyError, IndexError):
#     return {"img_url": None}
