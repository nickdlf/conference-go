from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(
                    o, property
                )  # since we referring to the name we pull the attr from the model class
                if property in self.encoders:  # if predefined in views encoder
                    encoder = self.encoders[property]
                    # Assigns LocationListEncoder() instance [name] in this case to key
                    value = encoder.default(value)
                    # calls default method on encoder w/ current value of property
                    # return value is then assigned back to value
                    # calls default method on LocationListEncoder -- this comes from ModelEncoder
                    # default method knows how to encode
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
        #   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        #   otherwise,
        #       return super().default(o)  # From the documentation


"""
For example, with the ConferenceDetailEncoder class
 and getattr(o, property), if property is "description", 
 Django will retrieve the value of the description field 
 from the o object, which represents an instance of the 
 Conference model. Django's model system takes care of mapping 
 the attribute to the underlying field value stored in the
  database.
"""
