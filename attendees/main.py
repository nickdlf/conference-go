## for JSON.py file
from json import JSONEncoder
from datetime import datetime


class CustomEncoder(JSONEncoder):
    def default(self, o):
        properties = ["name", "description"]  # can add anything that we want
        dict_to_encode = {}
        for prop in properties:
            dict_to_encode[prop] = getattr(
                o, prop
            )  # basically the same as x.foobar?
        return dict_to_encode
        # since prop is a variable we can't access it as a key? --> not sure on why we can't use o.prop
        #


class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateTimeEncoder, JSONEncoder):
    properties = []  # can add anything that we want

    def default(self, o):
        dict_to_encode = {}
        if isinstance(o, self.Conference):
            for prop in self.properties:
                dict_to_encode[prop] = getattr(
                    o, prop
                )  # basically the same as x.foobar?
            return dict_to_encode
        else:
            return super().default(o)
