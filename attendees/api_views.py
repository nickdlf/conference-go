from django.http import JsonResponse
from common.json import DateEncoder, ModelEncoder, QuerySetEncoder

from .models import Attendee
from events.models import Conference
from events.api_views import ConferenceListEncoder

from django.views.decorators.http import require_http_methods
import json


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
        "name",
        # "href",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        """
            1. - conference = conference_id?
        """
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeesListEncoder
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)

        # copied from get detail
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# ===============================================================================

# =============================LIST ATTENDEES===============
# """
# Lists the attendees names and the link to the attendee
# for the specified conference id.

# Returns a dictionary with a single key "attendees" which
# is a list of attendee names and URLS. Each entry in the list
# is a dictionary that contains the name of the attendee and
# the link to the attendee's information.
# """


# ===============================================================================

# =============================LIST ATTENDEES===============
#
# Returns the details for the Attendee model specified
# by the id parameter.

# This should return a dictionary with email, name,
# company name, created, and conference properties for
# the specified Attendee instance.
# """


"""
1.  How does conference=conference_id work?

    Django, when you define a foreign key field, Django automatically 
creates an additional field with the _id suffix to store the actual 
ID of the related object in the database. This automatically created 
field will have the same value as the id field of the related object.
    The _id field and the id field of the related object will
 have the same value in the database. Django handles the synchronization 
 between them when you create, update, or delete objects that are part of the 
 foreign key relationship.
"""
